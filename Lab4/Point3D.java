/**
 Zachary Gibson
 60975
*/

public class Point3D {
	public double x,y,z;
	
	public Point3D(double xx, double yy, double zz) {
		x = xx;
		y = yy;
		z = zz;
	}
}