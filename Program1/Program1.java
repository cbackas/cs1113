//====================================================================
// CS 1113 Program 1 : Mini Madlibs
// Semester : Fall
// Author: Zachary Gibson 
// Class CRN: 60975
// =====================================================================
import java.util.Scanner;
public class Program1 {
	public static void main(String[] args)
	{
		Scanner kb = new Scanner(System.in);

		System.out.println("Program 1 by Zac Gibson\n\n");

		System.out.println("Enter a adjective:");
		String adjective = kb.next();

		System.out.println("Enter a verb:");
		String verb = kb.next();

		System.out.println("Enter a temperature:");
		String temp = kb.next();
		
		System.out.printf("\n\nHere is your story:\nThere is a %s frog who lives near a pond. The frog likes to %s all day every day. Especially on days when the pond water is %s. ", adjective, verb, temp);
	}
}